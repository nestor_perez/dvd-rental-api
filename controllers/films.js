const { Pool } = require('pg');

function getFilms(req, res) {
    const pool = new Pool();
    var p = req.query.p;
    var r = req.query.r;

    if (!p || p < 0) {
        p = 1;
    }

    if (!r || r < 1 || r > 101) {
        r = 50;
    }

    const query = `
    SELECT
    COUNT(1) OVER() as count,
    fm.*, 
    row_to_json(lg.*) as language, 
    COALESCE(json_agg(DISTINCT ac.*) FILTER (WHERE ac.actor_id IS NOT NULL), '[]') as actor,
    COALESCE(json_agg(DISTINCT ca.*) FILTER (WHERE ca.category_id IS NOT NULL), '[]') as category
    FROM film fm
    LEFT JOIN language lg on lg.language_id=fm.language_id
    LEFT JOIN film_actor fa on fa.film_id = fm.film_id
    LEFT JOIN actor ac on ac.actor_id = fa.actor_id
    LEFT JOIN film_category fc on fc.film_id = fm.film_id
    LEFT JOIN category ca on ca.category_id = fc.category_id
    --WHERE fm.film_id = 257
    GROUP BY fm.film_id, lg.*
    ORDER BY fm.film_id ASC
    OFFSET ($1 * ($2-1)) LIMIT $1
    `;
    pool.query(query, [r, p], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

function getFilmsById(req, res) {
    const pool = new Pool();
    const id = req.params.id;

    const query = `SELECT 
    fm.*, 
    row_to_json(lg.*) as language, 
    COALESCE(json_agg(DISTINCT ac.*) FILTER (WHERE ac.actor_id IS NOT NULL), '[]') as actor,
    COALESCE(json_agg(DISTINCT ca.*) FILTER (WHERE ca.category_id IS NOT NULL), '[]') as category
    FROM film fm
    LEFT JOIN language lg on lg.language_id=fm.language_id
    LEFT JOIN film_actor fa on fa.film_id = fm.film_id
    LEFT JOIN actor ac on ac.actor_id = fa.actor_id
    LEFT JOIN film_category fc on fc.film_id = fm.film_id
    LEFT JOIN category ca on ca.category_id = fc.category_id
    WHERE fm.film_id = $1
    GROUP BY fm.film_id, lg.*
    ORDER BY fm.film_id ASC
    `;
    pool.query(query, [id], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

function getFilmsByCategoryId(req, res) {
    const pool = new Pool();
    var p = req.query.p;
    var r = req.query.r;

    if (!p || p < 0) {
        p = 1;
    }

    if (!r || r < 1 || r > 101) {
        r = 50;
    }
    const category_id = req.params.category_id;
    const query = `
    SELECT
    COUNT(1) OVER() as count,
    fm.*, 
    row_to_json(lg.*) as language, 
    COALESCE(json_agg(DISTINCT ac.*) FILTER (WHERE ac.actor_id IS NOT NULL), '[]') as actor,
    COALESCE(json_agg(DISTINCT ca.*) FILTER (WHERE ca.category_id IS NOT NULL), '[]') as category
    FROM film fm
    LEFT JOIN language lg on lg.language_id=fm.language_id
    LEFT JOIN film_actor fa on fa.film_id = fm.film_id
    LEFT JOIN actor ac on ac.actor_id = fa.actor_id
    LEFT JOIN film_category fc on fc.film_id = fm.film_id
    LEFT JOIN category ca on ca.category_id = fc.category_id
    WHERE ca.category_id = $1
    GROUP BY fm.film_id, lg.*
    ORDER BY fm.film_id ASC
    OFFSET ($2 * ($3-1)) LIMIT $2
    `;
    pool.query(query, [category_id, r, p], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

function getFilmsByLanguageId(req, res) {
    const pool = new Pool();
    const language_id = req.params.language_id;
    var p = req.query.p;
    var r = req.query.r;

    if (!p || p < 0) {
        p = 1;
    }

    if (!r || r < 1 || r > 101) {
        r = 50;
    }

    const query = `
    SELECT
    COUNT(1) OVER() as count,
    fm.*, 
    row_to_json(lg.*) as language, 
    COALESCE(json_agg(DISTINCT ac.*) FILTER (WHERE ac.actor_id IS NOT NULL), '[]') as actor,
    COALESCE(json_agg(DISTINCT ca.*) FILTER (WHERE ca.category_id IS NOT NULL), '[]') as category
    FROM film fm
    LEFT JOIN language lg on lg.language_id=fm.language_id
    LEFT JOIN film_actor fa on fa.film_id = fm.film_id
    LEFT JOIN actor ac on ac.actor_id = fa.actor_id
    LEFT JOIN film_category fc on fc.film_id = fm.film_id
    LEFT JOIN category ca on ca.category_id = fc.category_id
    WHERE lg.language_id = $1
    GROUP BY fm.film_id, lg.*
    ORDER BY fm.film_id ASC
    OFFSET ($2 * ($3-1)) LIMIT $2
    `;
    pool.query(query, [language_id, r, p], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

function getFilmsByActorId(req, res) {
    const pool = new Pool();
    const actor_id = req.params.actor_id;
    var p = req.query.p;
    var r = req.query.r;

    if (!p || p < 0) {
        p = 1;
    }

    if (!r || r < 1 || r > 101) {
        r = 50;
    }
    const query = `
    SELECT
    COUNT(1) OVER() as count,
    fm.*, 
    row_to_json(lg.*) as language, 
    COALESCE(json_agg(DISTINCT ac.*) FILTER (WHERE ac.actor_id IS NOT NULL), '[]') as actor,
    COALESCE(json_agg(DISTINCT ca.*) FILTER (WHERE ca.category_id IS NOT NULL), '[]') as category
    FROM film fm
    LEFT JOIN language lg on lg.language_id=fm.language_id
    LEFT JOIN film_actor fa on fa.film_id = fm.film_id
    LEFT JOIN actor ac on ac.actor_id = fa.actor_id
    LEFT JOIN film_category fc on fc.film_id = fm.film_id
    LEFT JOIN category ca on ca.category_id = fc.category_id
    WHERE ac.actor_id = $1
    GROUP BY fm.film_id, lg.*
    ORDER BY fm.film_id ASC
    OFFSET ($2 * ($3-1)) LIMIT $2
    `;
    pool.query(query, [actor_id, r, p], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

module.exports = { getFilms, getFilmsById, getFilmsByCategoryId, getFilmsByLanguageId, getFilmsByActorId }