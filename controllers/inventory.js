const {Pool} = require('pg');

function getInventoryByFilmId(req, res) {
    const pool = new Pool();
    const filmId = req.params.filmId;
    const storeId = req.jwt.sub.store_id;
    const query = `SELECT 
                    COUNT(iv.inventory_id),
                    iv.film_id
                    FROM inventory iv
                    LEFT JOIN rental rt on rt.inventory_id = iv.inventory_id and rt.return_date IS NULL
                    WHERE iv.film_id = $1 and rt.rental_id IS NULL and iv.store_id = $2
                    GROUP BY iv.film_id`;
    pool.query(query, [filmId, storeId], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send({msg: 'Servicio no disponible.'});
        }
        res.status(200).json(result);
    });
}

module.exports = {getInventoryByFilmId};