const {Pool} = require('pg');
const jwt = require('jsonwebtoken');
const authMiddleware = require('../middlewares/auth');

function login(req, res) {
    const pool = new Pool();
    const b64auth = (req.headers.authorization || '').split(' ')[1] || '';
    const [login, hashedPassword] = new Buffer(b64auth, 'base64').toString().split(':');
    const query = "SELECT * FROM Staff WHERE username = $1 and password = $2";
    pool.query(query, [login, hashedPassword], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(401).send('Usuario o contraseña incorrecta.');
        }
        delete result.rows[0]['password'];
        result.rows[0].claim = authMiddleware.claims.STAFF;
        return res.status(200).json({
            token: jwt.sign({
                sub: result.rows[0],
            }, 'dvd_demo_api', {expiresIn: "1d"})
        });
    });
}

function checkToken(req, res) {
    return res.status(200).json(req.jwt);
}

module.exports = {login, checkToken};