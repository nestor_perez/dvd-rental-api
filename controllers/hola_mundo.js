
/**
 * Esta función responde todas sus solicitudes con el mismo texto.
 * Es para fines demostrativos.
 * 
 * @param {Request} req Solicitud enviada por el cliente.
 * @param {Response} res Respuesto enviada al cliente
 */
function HolaMundo(req,res){
    res.send('Hello World');
}


/**
 * Se exportan las funciones que se requieran ocupar 
 * en otros archivos.
 */
module.exports = {HolaMundo};