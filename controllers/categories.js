const { Pool } = require('pg');

/**
 * Obtiene todas las categorias.
 * 
 * @param {Request} req Solicitud
 * @param {Response} res Respuesta
 */
function getCategories(req, res) {
    const pool = new Pool();
    pool.query("SELECT * FROM category", (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

/**
 * Obtiene solo una categoria por ID.
 * 
 * @param {Request} req Solicitud
 * @param {Response} res Respuesta
 */
function getCategoryById(req, res) {
    const pool = new Pool();
    const id = req.params.id;

    pool.query('SELECT * FROM category WHERE category_id = $1', [id], (err, result) => {
        pool.end();
        if (err) {
            console.log(err);
            return res.status(500).send('Servicio no disponible.');
        }
        if (result.rowCount === 0) {
            return res.status(404).send('No encontrado.');
        }
        res.status(200).json(result);
    });
}

module.exports = { getCategories, getCategoryById }