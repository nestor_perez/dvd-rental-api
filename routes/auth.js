var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth');

var AuthController = require('../controllers/auth');

router.get('/', AuthController.login);
router.get('/decode', auth.loginMiddleware, AuthController.checkToken);

module.exports = router;