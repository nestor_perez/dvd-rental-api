var express = require('express');
var router = express.Router();

var FilmsController = require('../controllers/films');

router.get('/', FilmsController.getFilms);
router.get('/:id', FilmsController.getFilmsById);
router.get('/category/:category_id', FilmsController.getFilmsByCategoryId);
router.get('/language/:language_id', FilmsController.getFilmsByLanguageId);
router.get('/actor/:actor_id', FilmsController.getFilmsByActorId);

module.exports = router;