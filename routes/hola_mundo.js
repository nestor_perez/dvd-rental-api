/**
 * Se importan los modulos necesarios.
 */
var express = require('express');
var router = express.Router();

/**
 * Se importa los controladores requeridos.
 */
var HolaMundoController = require('../controllers/hola_mundo');

/**
 * Se especifica con que metodo y a que ruta se accedera a cada controlador.
 */
router.get('/', HolaMundoController.HolaMundo);

/**
 * Se exporta este Ruteo, para su uso en la aplicación principal.
 */
module.exports = router;