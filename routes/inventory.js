var express = require('express');
var auth = require('../middlewares/auth');

var router = express.Router();

var InventoryController = require('../controllers/inventory');

router.get('/:filmId', auth.loginMiddleware, auth.hasClaim([auth.claims.STAFF]), InventoryController.getInventoryByFilmId);

module.exports = router;