var express = require('express');
var router = express.Router();

var CategoriesController = require('../controllers/categories');

router.get('/', CategoriesController.getCategories);
router.get('/:id', CategoriesController.getCategoryById);

module.exports = router;