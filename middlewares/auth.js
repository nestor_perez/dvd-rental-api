const jwt = require('jsonwebtoken');
const claims = {STAFF: 'staff'};

function loginMiddleware(req, res, next) {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        jwt.verify(req.headers.authorization.split(' ')[1], 'dvd_demo_api', (err, decode) => {
            if (err) return res.status(401).send({msg: 'Inicio de sesión requerido.'});
            req.jwt = decode;
            next();
        });
    } else {
        return res.status(401).send('Inicio de sesión requerido.');
    }
}

function hasClaim(role) {
    return function (req, res, next) {
        var pass = false;
        role.forEach((item) => {
            if (item === req.jwt.sub.claim) {
                pass = true;
            }
        });

        if (pass) {
            next();
        } else {
            return res.status(403).send('Permisos insuficientes para acceder a este recurso.');
        }
    }
}

module.exports = {loginMiddleware, hasClaim, claims};