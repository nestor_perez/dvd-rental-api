/**
 * Importación de modulos requeridos por app.js
 */
var express = require('express');
var app = express();

/**
 * Importación de las rutas.
 */
const HolaMundoRoute = require('./routes/hola_mundo');
const CategoriesRoute = require('./routes/categories');
const FilmsRoute = require('./routes/films');
const AuthRoute = require('./routes/auth');
const InventoryRoute = require('./routes/inventory');


app.use('/hola_mundo', HolaMundoRoute);
app.use('/categories', CategoriesRoute);
app.use('/films', FilmsRoute);
app.use('/auth', AuthRoute);
app.use('/inventory', InventoryRoute);

/**
 * Puerto de escucha de nuestra API.
 */
const PORT = process.env.PORT || 8080;
const server = app.listen(PORT, () => {
    console.log(`Escuchando en puerto: ${server.address().port}`);
});
